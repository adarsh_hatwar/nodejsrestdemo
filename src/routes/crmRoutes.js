import {
    addNewContact,
    getContacts,
    getContactById,
    updateContact,
    deleteContact
} from '../controllers/crmController';

const routes = (app) => {
    app.route('/contacts')
        .get((req, res, next) => {
            // middleware
            console.log(`Request URL: ${req.originalUrl}`);
            console.log(`Request Method: ${req.method}`);
            next();
        }, getContacts)

        .post(addNewContact);

    app.route('/contact/:contactId')
        .get(getContactById)

        .put(updateContact)

        .delete(deleteContact);

}

export default routes;