import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import routes from './routes/crmRoutes';

const app = express();
const PORT = 3000;
const MONGO_USER = 'admin'
const MONGO_PASSWORD = '!t!(entr@lSKUL'
const MONGO_URI = `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@nodejsdemo-zh6mt.mongodb.net/test?retryWrites=true&w=majority`


// mongoose connection
mongoose.Promise = global.Promise;
mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// body parser setup
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

routes(app);

// serve static files
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.send(`Node express server running on port ${PORT}`);
});

app.listen(PORT, () => {
    console.log(`Your server is listening on port ${PORT}`);
});


